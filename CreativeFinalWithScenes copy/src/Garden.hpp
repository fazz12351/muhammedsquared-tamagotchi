//
//  Garden.hpp
//  CreativeFinalWithScenes copy
//
//  Created by Mohammed Ahmed on 17/03/2021.
//

#ifndef Garden_hpp
#define Garden_hpp

#include <stdio.h>
#include "ofMain.h"

#endif /* Garden_hpp */

class Garden: public ofBaseApp{

    public:
        void setup();
        void draw();

        void swings(int num);
        void scenary(int num);

        ofEasyCam camera;
        ofLight keyLight;
        
        //swings
        ofCylinderPrimitive pole1, pole2, pole3, pole4, pole5;
        ofBoxPrimitive rope1, rope2, rope3, rope4, seat1, seat2;
        
        //scenary
        ofTexture ground_texture, fence_texture, rope_texture, pole_texture, seat_texture;
        ofBoxPrimitive ground, fence1, fence2, fence3;
};
