//
//  Garden.cpp
//  CreativeFinalWithScenes copy
//
//  Created by Mohammed Ahmed on 17/03/2021.
//

#include "Garden.hpp"


//--------------------------------------------------------------
void Garden::setup(){

    //we setup our garden scene here including the int value of the swings and scene
    ofSetBackgroundColor(30);
    swings(1);
    scenary(1);

    //we load the textures here
    ofDisableArbTex();
    ofLoadImage(ground_texture, "grass.jpg");
    ofLoadImage(fence_texture, "fence.png");
    ofLoadImage(pole_texture, "pole.jpg");
    ofLoadImage(seat_texture, "seat.jpg");
    ofLoadImage(rope_texture, "rope.jfif");
}



//--------------------------------------------------------------
void Garden::draw(){
    //this draw function will be called in the ofapp.cpp

    ofEnableDepthTest();

    swings(2);
    scenary(2);

//    camera.end();
    ofDisableDepthTest();
}

//------------------------------------



//--------------------------------------------------------------
void Garden::swings(int num) {
    if (num == 1)
    {
        //we set the swings pos,size ect... We set the parent cell th pole1 and rope1
        pole2.setParent(pole1), pole3.setParent(pole1), pole4.setParent(pole1), pole5.setParent(pole1);
        rope2.setParent(rope1), rope4.setParent(rope3), seat1.setParent(rope1), seat2.setParent(rope3);

        pole1.setPosition(0, -50, -20);
        pole2.setPosition(-50, 0, 0);
        pole3.setPosition(-50, 8, 0);
        pole4.setPosition(150, 1.5, 0);
        pole5.setPosition(150, 1.5, 0);

        //we set the heights and radius of the poles This will be used for the GUI of the primitives.
        pole1.setHeight(200), pole1.setRadius(2), pole1.setResolution(100, 100, 100);
        pole2.setHeight(100), pole2.setRadius(2), pole2.setResolution(100, 100, 100);
        pole3.setHeight(100), pole3.setRadius(2), pole3.setResolution(100, 100, 100);
        pole4.setHeight(100), pole4.setRadius(2), pole4.setResolution(100, 100, 100);
        pole5.setHeight(100), pole5.setRadius(2), pole5.setResolution(100, 100, 100);

        
//we set the posision of the 3d objects
        rope1.setPosition(-10, -37.5, -20);
        rope2.setPosition(20, 0, 0);
        rope3.setPosition(90, -37.5, -20);
        rope4.setPosition(20, 0, 0);
//we sset the width of the 3d object...
        rope1.setWidth(2), rope1.setDepth(2), rope1.setHeight(75);
        rope2.setWidth(2), rope2.setDepth(2), rope2.setHeight(75);
        rope3.setWidth(2), rope3.setDepth(2), rope3.setHeight(75);
        rope4.setWidth(2), rope4.setDepth(2), rope4.setHeight(75);

        seat1.setPosition(10, -39, 0);
        seat2.setPosition(10, -39, 0);

        seat1.setWidth(30), seat1.setDepth(10), seat1.setHeight(3);
        seat2.setWidth(30), seat2.setDepth(10), seat2.setHeight(3);
    
    }
    //this condtional statemnt is used so when the function with the argument 1 or 2, will display differnt scenes.

    if (num == 2)
    {
        ofPushMatrix();
        ofTranslate(-200, 0, -150);

        ofPushMatrix();
        ofRotateDeg(90);
        pole_texture.bind();
        pole1.draw();
        pole_texture.unbind();
        ofPopMatrix();

        ofPushMatrix();
        ofRotateXDeg(10);
        pole_texture.bind();
        pole2.draw();
        pole4.draw();
        ofRotateXDeg(335);
        pole3.draw();
        pole5.draw();
        pole_texture.unbind();
        ofPopMatrix();

        rope_texture.bind();
        rope1.draw();
        rope2.draw();
        rope3.draw();
        rope4.draw();
        rope_texture.unbind();

        seat_texture.bind();
        seat1.draw();
        seat2.draw();
        seat_texture.unbind();

        ofPopMatrix();
    }
}


void  Garden::scenary(int num)
{
    //we set the scenery here.. IF num ==1 we execute the following code
    if (num == 1)
    {
        ground.setPosition(0, -100, 0);

        ground.setWidth(600), ground.setHeight(2), ground.setDepth(600);

        fence1.setParent(ground);
        fence2.setParent(ground);
        fence3.setParent(ground);

        fence1.setPosition(0, 37.5, -300);
        fence2.setPosition(-300, 37.5, 0);
        fence3.setPosition(300, 37.5, 0);


        fence1.setWidth(600), fence1.setHeight(75), fence1.setDepth(1);
        fence2.setWidth(1), fence2.setHeight(75), fence2.setDepth(600);
        fence3.setWidth(1), fence3.setHeight(75), fence3.setDepth(600);
    }

    //if num==2 we execute a different scene. This is for scene management.
    if (num == 2)
    {
        ground_texture.bind();
        ground.draw();
        ground_texture.unbind();

        fence_texture.bind();
        fence1.draw();
        fence2.draw();
        fence3.draw();
        fence_texture.unbind();

    }

}
