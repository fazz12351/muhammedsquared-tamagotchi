//
//  Globe.cpp
//  teapotScene
//
//  Created by Mohammed Ahmed on 16/02/2021.
//

#include "Globe.hpp"


Globe::Globe(){
    //we set the globe skeleton at the centre of the canvas. in the ofApp.cpp we will alter the posison using ofPushMatrix(),ofTranslate(),ofPopMatrix;
    ofDisableAlphaBlending();
    skeleton.setPosition(0, 0, 0);
    skeleton.setRadius(15);
    ofDisableArbTex();
    ofLoadImage(texture, "earth2.png");
    light.enable();
    light.setPosition(0, 0, 0);
    light.lookAt(ofVec3f(0,0,0));

}



void Globe::draw(){
    //this will be called in the . We bind the texture to the 3d primitive.
    texture.bind();
    skeleton.draw();
    texture.unbind();
}
