//
//  HealthBar.cpp
//  teapotScene
//
//  Created by Mohammed Ahmed on 17/02/2021.
//

#include "HungerBar.hpp"

Hunger::Hunger(){
    //we set up our hunger variavlrs textures and timer here. Hunger value simmilary to healthbar is used to create the bar affecct when drawing the rectangle
    HungerValue=150;
    petinteract=false;
    Timer=0;
    maxSound.load("happy.wav");
    maxSound.setVolume(0.6);
}

void Hunger::update(){
    //the timer is used to alter the Hunger Value over time. Currently it is incrementing by 0.03 per framew.
    Timer+=0.03;
    if(petinteract==true){
        HungerValue+=1;
    }
    //when the interactions of the fridge stops and the timer is greater than 10. the timer resets and the Hunger value slowly decrements
    if(petinteract==false&&Timer>=10){
        HungerValue-=20;
        Timer=0;
    }

    
    
}

void Hunger::draw(){
    
    //this draws the text on the canvas illustrating wehat the bar is to users
    ofDrawBitmapString("HUNGER", ofGetWidth()/2-350, 90);
    
    //this forloop will be used to draw a rectangle with differnt x pos according to the hunger value variable.
    for(int i=0;i<HungerValue;i++){
        ofSetColor(255,255,100);
        ofDrawRectangle(ofGetWidth()/2-380+i, 100, 20, 20);
        
        //this conditional statment displays text when the pet is full
        if(HungerValue>=280){
            ofSetColor(90,255,0);
            HungerValue=280;
            ofDrawBitmapString("YAYY", ofGetWidth()/2-350, 140);
            maxSound.play();
            
        }
        //displays text whren pet is hungry.
        if(HungerValue<=0){
            ofDrawBitmapString("HMM So You Dont Need To Feed Me :(", ofGetWidth()/2-350, 140);
        }
    }
    
    

    
  
    
   

    
    
}

