//
//  HealthBar.cpp
//  teapotScene
//
//  Created by Mohammed Ahmed on 17/02/2021.
//

#include "HealthBar.hpp"

Health::Health(){
    //se set up our animal health float value here. This will be used to control the value pof the health bat. When the bool is true, it will increment the Health vvalue creating a bar affect
    AnimalHealth=150;
    petinteract=false;
    //we load the sound file here
    maxHealth.load("health.aup");
    myfont.load("PressStart2P-Regular.ttf", 32);

}

void Health::update(){
    //this is update function which controls the petinteract boolean. When it is true, the health value increment and when false, it decrements.
    if(petinteract==true){
        AnimalHealth+=1;
    }
    if(petinteract==false){
        AnimalHealth-=0.00001;
    }
    
    
}

void Health::draw(){
    ofDrawBitmapString("EMOTIONS", ofGetWidth()/2, 90);
//    myfont.drawString("EMOTIONS", ofGetWidth()/2, 90);
//    myfont.drawString(ofToString("EMOTIONS"), 800, 40);
    
    for(int i=0;i<AnimalHealth;i++){
        ofPushMatrix();
//        ofFill();
//        ofSetColor(0,255,100);
        if(AnimalHealth<=400){
            ofDrawRectangle(ofGetWidth()/2-50+i, 100, 20, 20);
        }
        if(AnimalHealth>=400){
            AnimalHealth=400;
            ofDrawRectangle(ofGetWidth()/2-50, 100, AnimalHealth, 20);
            ofDrawBitmapString("IM REALLY HAPPY", ofGetWidth()/2, 140);
            
        }
        
        ofPopMatrix();
        

    }
    
   

    
    
}

