//
//  Paintings.cpp
//  teapotScene
//
//  Created by Mohammed Ahmed on 15/02/2021.
//

#include "Paintings.hpp"


Painting::Painting(){
    
    //here we set up each frame of SceneCounter2. We initilise the x,y,z,withdth heeight ect.
    Frame.setPosition(30, 0,0);
    Frame.setDepth(4);
    Frame.setWidth(50);
    
    //setting the posision of frame1 and its variables
    Frame1.setPosition(-90, 0,0);
    Frame1.setDepth(4);
    Frame1.setWidth(50);
    
    //setting the posision of frame2 and its variables
    Frame2.setPosition(-125, 0,-60);
    Frame2.setDepth(50);
    Frame2.setWidth(4);
    
    //setting the posision of frame3 and its variables
    Frame3.setPosition(-125, 0,-140);
    Frame3.setDepth(50);
    Frame3.setWidth(4);
    
    //setting the posision of frame4 and its variables
    Frame4.setPosition(-125, 0,-220);
    Frame4.setDepth(50);
    Frame4.setWidth(4);
    
    //setting the posision of frame5 and its variables
    Frame5.setPosition(30, 0,-275);
    Frame5.setDepth(4);
    Frame5.setWidth(50);
    
    //setting the posision of frame6 and its variables
    Frame6.setPosition(-90, 0,-275);
    Frame6.setDepth(4);
    Frame6.setWidth(50);
    
    //setting the posision of frame7 and its variables
    Frame7.setPosition(80, 0,-60);
    Frame7.setDepth(50);
    Frame7.setWidth(4);
    
    //setting the posision of frame8 and its variables
    Frame8.setPosition(80, 0,-140);
    Frame8.setDepth(50);
    Frame8.setWidth(4);
    
    //setting the posision of frame9 and its variables
    Frame9.setPosition(80, 0,-220);
    Frame9.setDepth(50);
    Frame9.setWidth(4);
    
    //we load our textures of each paining here
    
    ofLoadImage(texture, "Mona.jpg");
    ofLoadImage(texture1, "Mona1.jpg");
    ofLoadImage(texture2, "Mona2.jpg");
    ofLoadImage(texture3, "Mona3.jpg");
    ofLoadImage(texture4, "Mona4.jpg");
    ofLoadImage(texture5, "Mona5.jpg");
    ofLoadImage(texture6, "Mona6.jpg");
    ofLoadImage(texture7, "Mona7.jpg");
    ofLoadImage(texture7, "Mona8.jpg");
    ofLoadImage(door, "door.jpg");
    
}


void Painting::draw(){
    //we will draw all our 3d objects. All object had got there own properties and will be drawn ocordingly.
    texture.bind();
    Frame.draw();
    texture.unbind();
    
    //we bind and draw our 3d object
    texture1.bind();
    Frame1.draw();
    texture1.unbind();
    
    //we bind and draw our 3d object
    texture2.bind();
    Frame2.draw();
    texture2.unbind();
    
    //we bind and draw our 3d object
    texture3.bind();
    Frame3.draw();
    texture3.unbind();
    
    //we bind and draw our 3d object
    texture4.bind();
    Frame4.draw();
    texture4.unbind();
    
    //we bind and draw our 3d object
    texture5.bind();
    Frame5.draw();
    texture5.unbind();
    
    //we bind and draw our 3d object
    texture6.bind();
    Frame6.draw();
    texture6.unbind();

    //we bind and draw our 3d object
    texture7.bind();
    Frame7.draw();
    texture7.unbind();
    
    //we bind and draw our 3d object
    texture8.bind();
    Frame8.draw();
    texture8.unbind();
    
    //we bind and draw our 3d object
    door.bind();
    Frame9.draw();
    door.unbind();
}
