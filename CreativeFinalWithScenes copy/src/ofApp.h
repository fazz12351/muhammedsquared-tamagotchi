
#pragma once

#include "ofMain.h"
#include "Paintings.hpp"
#include "Globe.hpp"
#include "CatFiles.hpp"
#include "HealthBar.hpp"
#include "HungerBar.hpp"

#include "kitchen.h" //allows me to use the kitchen header file ive created
#include "kitchen_cupboard.h" //allows me to use the kitchen_cupboard header file ive created
#include "kitchen_draw.h" //allows me to use the kitchen_draw header file ive created
#include "kitchen_microwave.h" //allows me to use the kitchen_microwave header file ive created
#include "kitchen_oven.h" //allows me to use the kitchen_oven header file ive created
#include "kitchen_fridge.h" //allows me to use the kitchen_fridge header file ive created
#include "ofxGui.h" //allows me to add onscreen sliders to manipulate the lighting
#include "Garden.hpp"
using namespace glm;

class ofApp : public ofBaseApp{

    public:
        void setup();
        void update();
        void draw();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y );
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);
    void TeapotAndPedestalSetup();
    void TeapotAndPedestalDraw();
    void CameraAndLightingSetup();
    void addQuadFacet(ofMesh &curMesh, vec3 v0, vec3 v1, vec3 v2, vec3 v3, vec2 tMin, vec2 tMax);
    void StartGame();
    void KitchenSetup();
    void KitchenUpdate();
    void KitchenDraw();
    void CatRange();
    bool kitchenInteractions;
    
    ofEasyCam camera;
    ofLight keyLight;
    ofVboMesh teapotMesh,teapotMesh1;
    ofVboMesh createCuboidMesh(float width, float height);
    ofVboMesh pedestaMesh,pedestaMesh1;
    ofMaterial pedestaMaterial,pedestaMaterial1;
    ofImage woodTexture;
    ofMaterial teapotMaterial,teapotMaterial1;
    ofLight light;
    ofTexture texture,doors1;
    ofVec3f position;
    
    ofBoxPrimitive floor;
    //class being called
    Painting myPaint;
    Globe myGlobe;
    Cat myCat;
    Health pethealth;
    Hunger pethunger;
  
    
    
    ofSoundPlayer intro;
    
    
    bool interact;
    bool roomlight;
    bool petinteraction;
    
    int SceneCounter;
    int red,green,blue;
    
    float number1,number2,DeathTimer;
    int rotateGlobe;
    int kitchenObj;
 

    kitchen room; //draws the room of the kitchen
    kitchen_cupboard cupboard, cupboard2, cupboard3; //draws the cupboards
    kitchen_draw kit_draw, kit_draw2, kit_draw3, kit_draw4; //draws the kitchen draws
    kitchen_microwave micro; //draws the microwave
    kitchen_oven oven; //draws the oven #
    kitchen_fridge fridge; //draws the fridge

    ofxPanel gui; //draws the gui
    ofxFloatSlider light_red, light_blue, light_green; //creates sliders that uses float values

    ofxVec3Slider light_pos; //creates a slider that uses vec3 numbers
    
    Garden Garden;
    
    
    ofTrueTypeFont myfont;

    
        
};
