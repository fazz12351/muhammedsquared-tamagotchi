#include "kitchen_fridge.h"

kitchen_fridge::kitchen_fridge()
{
    
	//-------------------------- setting parrents ---------------------------\\
	//this section sets the their parent to "handle3" 
	handle1.setParent(handle3);
	handle2.setParent(handle3);
	
	//this section sets the their parent to "door" 
	handle3.setParent(door);
	second_door.setParent(door);
	body.setParent(door);

	//this section sets the their parent to "second_handle3" 
	second_handle1.setParent(second_handle3);
	second_handle2.setParent(second_handle3);
	
	//sets second_handle3's parent to "second_door"
	second_handle3.setParent(second_door);
    




	//-------------------------- setting position ---------------------------\\
	
	//this section is setting the positions of each object 
	door.setPosition(0, -35, 149);
	body.setPosition(13.5,0,-15);
	handle1.setPosition(0, -24, -1);
	handle2.setPosition(0, 24, -1);
	handle3.setPosition(10, 10, 4);	
	
    //setting the posision of the fridge elemtns
	second_door.setPosition(28,0,0);
	second_handle1.setPosition(0, -24, -1);
	second_handle2.setPosition(0, 24, -1);
	second_handle3.setPosition(-10, 10, 4);
    
    fridgeBody.setPosition(0, -130, 0);
    fridgeBody1.setPosition(0, -130, 0);

    door1.setPosition(-20,-30,230);
    door2.setPosition(-20,-30,330);
    
    

	//-------------------------- setting width, height, depth ---------------------------\\

	//this section is dedicated to setting the width, height and depth for each object 
	door.setWidth(25), door.setHeight(150), door.setDepth(3);
	body.setWidth(60), body.setHeight(150), body.setDepth(30);
	handle1.setWidth(0.8), handle1.setHeight(0.8), handle1.setDepth(2);
	handle2.setWidth(0.8), handle2.setHeight(0.8), handle2.setDepth(2);
	handle3.setHeight(54), handle3.setRadius(0.5), handle3.setResolution(100, 100, 100);
    fridgeBody.setHeight(250);
    fridgeBody1.setHeight(200);
	
    //setting the width and height of the object
	second_door.setWidth(25), second_door.setHeight(150), second_door.setDepth(3);
	second_handle1.setWidth(0.8), second_handle1.setHeight(0.8), second_handle1.setDepth(2);
	second_handle2.setWidth(0.8), second_handle2.setHeight(0.8), second_handle2.setDepth(2);
	second_handle3.setHeight(54), second_handle3.setRadius(0.5), second_handle3.setResolution(100, 100, 100);
    fridgeBody.setWidth(600);
    fridgeBody1.setWidth(550);
    
    door1.setWidth(8);
    door1.setDepth(50);
    
    door2.setWidth(8);
    door2.setDepth(50);
  
	
	//-------------------------- applying textures ---------------------------\\

	ofDisableArbTex();
	ofLoadImage(door_texture, "oven_body.png");
    ofLoadImage(fridgetexture,"fridgeinside.jpg");
    ofLoadImage(appletexture,"apple.jpg");
    ofLoadImage(doors1,"door.jpg");
    

	//-------------------------- applying materials ---------------------------\\
	
	//this section is used to alter each material's features 
	door_material.setDiffuseColor(ofFloatColor(1.0, 1.0, 1.0));
	door_material.setSpecularColor(ofFloatColor(0.7, 0.7, 0.7));
	door_material.setShininess(50.0);
	door_material.setAmbientColor(ofFloatColor(0.4, 0.4, 0.4));

	handle_material.setDiffuseColor(ofFloatColor(1.0, 1.0, 1.0));
	handle_material.setSpecularColor(ofFloatColor(0.7, 0.7, 0.7));
	handle_material.setShininess(50.0);
	handle_material.setAmbientColor(ofFloatColor(0.4, 0.4, 0.4));
    
    apple.setRadius(70);

}

kitchen_fridge::~kitchen_fridge()
{
	
}


//function is used to set the position inside the ofApp.cpp file
void kitchen_fridge::setPosition(float x, float y, float z)
{
	if (x < 5000 && x > 0) 
	
	posX = x;
	posY = y;
	posZ = z;

	door.setPosition(posX, posY, posZ);

}


void kitchen_fridge::update()
{

}

void kitchen_fridge::draw()
{
    
	//this section draws the doors and body and applies its materials and texture to them
	door_material.begin();
	door_texture.bind();
	body.draw();
	door.draw();	
	second_door.draw();
	door_texture.unbind();
	door_material.end();

	handle_material.begin();

	handle1.draw();
	handle2.draw();
	handle3.draw();	
	
	second_handle1.draw();
	second_handle2.draw();
	second_handle3.draw();

	handle_material.end();
}



void kitchen_fridge::opened(){
    //this is the scene where the cat goes into the fridge. We call this function
    ofBackground(0);
    ofPushMatrix();
    body.draw();
    ofPopMatrix();
    door_texture.unbind();
    ofDrawBitmapString("Press Right arrow to return to Kitchen", 0, 0);

    
}
void kitchen_fridge::doors(){
    //we draw the doors ehich lead back towarsdfs the art gallery and garden
    doors1.bind();
    door1.draw();
    door2.draw();
    doors1.unbind();
    
   
}

