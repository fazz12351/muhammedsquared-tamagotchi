
//  Created by Mohammed Ahmed on 05/02/2021.
//

//get x and y value of floor. Add width of floor to the pos.x. Use conditional statemetn to check if the cat  pos.x == floor.pos.x

#include "CatFiles.hpp"

Cat::Cat(){
    //we have a timer =0; this will be used to manipulate between the egg hatching scene. At diffderent times, new textuers will be added.
    timer=0;
    //we use this vecot to put the posision of the egg at the center of the canvas
    pos.x=ofGetWidth()/2,
    pos.y=ofGetHeight()/2;
    //this posision is for the cat in the game scene. this will change over time unlinked pos
    posision.x=0;
    posision.y=0;
    posision.z=-50;
  //this allows the textutre to be added which enables the texture to look flush.
    ofDisableAlphaBlending();
    ofEnableDepthTest();
    ofDisableArbTex();
    ofDisableAlphaBlending();
    //we enable lighting here
    light.enable();
    light.setPosition(ofGetWidth()/2, 50, 200);
    head.setPosition(pos.x, pos.y, 0);
    //we load our texture here :
    ofLoadImage(texture, "cat.jpg");
    ofLoadImage(texture1, "eggcrack.jpg");
    ofLoadImage(texture2, "eggcrack1.jpg");
    ofLoadImage(texture3, "eggcrack2.jpg");
    ofLoadImage(texture4, "eggcrack3.jpg");
    ofLoadImage(texture5, "eggcrack4.jpg");
    head.setRadius(70);
    
    
    
    
    

}

void Cat::menuScene(){
    //this sceen will be called when SceneCounter =0 in ofApp.cpp. This scene displays a pet in the centre of the canvas
    ofPushMatrix();
    ofFill();
    ofSetColor(255);
    ofDrawBitmapString("Press Mouse to Start Game", pos.x-100, pos.y+100);
    ofPopMatrix();
    ofPushMatrix();
    texture.bind();
    head.draw();
    texture.unbind();
    ofPopMatrix();

    

    
}

void Cat ::eggHatchScene(){
    //this scene will be called when SceneCounter ==1. You ca see we use different timer value to manipulate through different textures over time
    ofPushMatrix();
    ofDrawBitmapString("Give me a sec whilst i break out of HEREEEEEE!!!", ofGetWidth()/2, 20);
    timer+=0.1;
    
    if(timer <=5){
        texture1.bind();
    }
    if(timer >=5&&timer<=10){
        texture2.bind();
    }
    if(timer >=10&&timer<=15){
        texture3.bind();
    }
    if(timer >=15&&timer<=20){
        texture4.bind();
    }
    if(timer >=20){
        cam.setDistance(200);
        texture5.bind();
        
    }
    //we draw our primitive with our textures binded.
    ofRotateXDeg(-35);
    ofRotateYDeg(185);
    head1.draw();
    texture1.unbind();
    texture2.unbind();
    texture3.unbind();
    ofPopMatrix();
 

    
}

void Cat::game(){
    

    head.setRadius(10);
    head.setPosition(posision.x, posision.y, posision.z);
    texture.bind();
    head.draw();
    texture.unbind();
    
      
    


    
}
void Cat::VirtualPos(){
    ofDrawBitmapString(posision.x,ofGetWidth()/2-500, 20);
    ofDrawBitmapString(posision.y,ofGetWidth()/2-500, 40);
    ofDrawBitmapString(posision.z,ofGetWidth()/2-500, 60);
    
    if(posision.x>=-120&&posision.x<=120){
//        std::cout << "In pos" << endl;
    }
    
    else{
        std::cout << "out of pos" << endl;
        posision.x=0;
    
    }
    
    if(posision.z>=-232&&posision.z<=40){
//        std::cout << "In pos" << endl;
    }
    else{
//        std::cout << "out of pos" << endl;
        posision.z=0;
    
    }
    
    

    
    
}


