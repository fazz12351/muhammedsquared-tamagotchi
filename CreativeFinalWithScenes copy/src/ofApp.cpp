
#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    //we set the background to black
    ofSetBackgroundColor(0);
    //we load sound our sound with the variable called intro and set the volume to 0.6. As this is a short sound clip, we loop it.
    intro.load("tamagotchi.wav");
    intro.setVolume(0.6);
    intro.play();
    intro.setLoop(true);
    
    //we load our Tpot mesh and textures here;
    teapotMesh.load("teapot.ply");
    teapotMesh1.load("teapot.ply");
    ofLoadImage(texture, "floor.jpg");
    ofLoadImage(doors1, "door.jpg");
    
    //we set the posision of the floor. we havent created a seperate class as its a simple primitive. we use these vectors to display the posision of the floor.
    position.x=0;
    position.y=-70;
    position.z=-95;
    //we set the posision of the floor and the width and height.
    floor.setPosition(position.x, position.y, position.z);
    floor.setDepth(270);
    floor.setHeight(5);
    floor.setWidth(200);
   
    //we setup our camera using the function which has all our camera setup data.
    CameraAndLightingSetup();
    //we setup or teapot and pedalsta and call the function for setup
    TeapotAndPedestalSetup();
    
    //we call our kitchen setup here
    KitchenSetup();
    
    //this boolean controls the globe in SceneCounter1. When it is true, it will ortate on its axis. We also have a boolean for roomlight, when user clicks up or down arrow, the variables become true and turn the light on to pos 0,0;
    interact=false;
    roomlight=false;
    petinteraction=false;
    kitchenInteractions=false;
    
    //we set the int of kitchen object and SceneCounter  to 0, these variables will be used to manipulate between scenes.
    kitchenObj=0;
    //this manages each scene.
    SceneCounter = 0;
    //this manages the time of which the health bars go low, it will increment. When it reaches above 15,game will end.
    DeathTimer=0;
    //we load our font.
    myfont.load("PressStart2P-Regular.ttf", 32);
    
    //we setup our garden class object here
    Garden.setup();

}

//--------------------------------------------------------------
void ofApp::update(){
//    //this will show the light on the cameraview. when the bool for the roomlight becomes true, we set the light up which follows tthe camera posision.
    if(roomlight==true){
        ofVec3f cpos = camera.getPosition();
        ofVec3f lpos=light.getPosition();
        ofVec3f delta=cpos-lpos;
        ofVec3f fin(100,100,100);
      //this ensures that the lighting doesnt instalty rotate toweards the camera angle. MAke sure there is a delay when switching between angles.
        if(delta.length()>5){
            light.setPosition(lpos+delta/fin);
        }
    }
    //if the boolean is true, we setr the posision of the light on the rooms itself.
    if(roomlight==false){
        light.setPosition(0, 0, 0);
    }
    
    //we call the health class update function.
    pethealth.update();
    pethunger.update();
  
    

}

//--------------------------------------------------------------
void ofApp::draw(){
//when SceneCounter is 0, we display the menu scene form the class myCat. The MenuScene Function allows the user to delect the pet.
    if(SceneCounter == 0){
        ofBackground(0);
        myCat.menuScene();
    }
    //When SceneCounter ==1(Egg hatch Scene) we setup the camera and initilize the disrtance and draw the cat object onto the scene. We also have a timer which will increment in SceneCounter2. When it hits a spicif number, it will move to the next Scene. The timer is used to maniplate texture and affect of each primitive.
    if(SceneCounter == 1&&kitchenObj==0){
        ofBackground(0);
        ofPushMatrix();
        intro.setVolume(0.05);
        camera.begin();
        camera.setDistance(0);
        camera.setDistance(100);
        myCat.eggHatchScene();
        camera.end();
        ofPopMatrix();
        if(myCat.timer>=25){
            SceneCounter= 2;
        }
    }
    
    //This controls the functions of the art gallery, when the SceneCoutner ==2, it will call this aswell kitcehn object. If kitchen object is ==0; no objects in the kitchen will be displayed in first person view.
    //we call our timer,health bar fridge functions to this scene. II have created multiple classes and function which allow users to re use the code for future use.
    if(SceneCounter==2&&kitchenObj==0){
        ofBackground(0);
        ofPushMatrix();
        myCat.timer=0;
        pethealth.draw();
        pethunger.draw();
        fridge.doors();
        StartGame();
        //this checks th evalue of the "emotion Bat" and "Hunget Bar'" if both values are less than=0, we cerate a camera affect which spazzes the pet. This ensures the iuser that both hunet value and emtoion value has gone down and requires attention
        if(pethunger.HungerValue<=0&&pethealth.AnimalHealth<=0){
            DeathTimer+=0.01;
            camera.setDistance(ofRandom(200,220));
            ofDrawBitmapString("You Dont love me?",0,150);
            intro.setVolume(0.2);
            //when each bars have gone below a certain point, we start tje Death timer. If it gets above 20.0, the game will end and intro sound will stop.
            if(DeathTimer>=20.0){
                ofBackground(0);
                camera.end();
                ofDrawBitmapString("GAME OVER", ofGetWidth()/2, ofGetHeight()/2);
                intro.stop();
            }
        }
        ofPopMatrix();
    }
    
    
    //posision of the cat on the 3d canvas
    if(SceneCounter == 3&&kitchenObj==0){
        ofBackground(0);
        pethealth.draw();
        pethunger.draw();
        ofPushMatrix();
        ofTranslate(0, 0,0);
        ofNoFill();
        KitchenDraw();
        ofNoFill();
        ofPopMatrix();
        //this checks th evalue of the "emotion Bat" and "Hunget Bar'" if both values are less than=0, we cerate a camera affect which spazzes the pet. This ensures the iuser that both hunet value and emtoion value has gone down and requires attention
        if(pethunger.HungerValue<=0&&pethealth.AnimalHealth<=0){
            DeathTimer+=0.01;
            camera.setDistance(ofRandom(200,220));
            ofDrawBitmapString("You Dont love me?",myCat.posision.x,myCat.posision.y);
            intro.setVolume(0.2);
            //when each bars have gone below a certain point, we start tje Death timer. If it gets above 20.0, the game will end and intro sound will stop.
            if(DeathTimer>=10.0){
                ofBackground(0);
                camera.end();
                ofDrawBitmapString("GAME OVER", ofGetWidth()/2, ofGetHeight()/2);
                intro.stop();
            }
        }

    }
    //if the SceneCounter is ==4, the class function of the garden will be drawn and mycat will also be draw onto the garden scene
    if(SceneCounter == 4){
        ofBackground(0);
        camera.begin();
        Garden.draw();
        myCat.game();
        camera.end();
//        myCat.VirtualPos();
        CatRange();
    }
    
    //this controls the interaction of the fridge object in Scene3(Kitchen). When kitchen object ==1(Pet goes towards the fridge), this will draw the fridge in and only the fridge with the hunger bar ontop. This is so it focuses on the user feeding the pet.
    if(kitchenObj == 1 && SceneCounter==3){
        pethunger.draw();
        camera.begin();
        fridge.draw();
        camera.end();
        if(kitchenInteractions==true){
            pethunger.petinteract=true;
            fridge.opened();
            ofDrawBitmapString("Click Right Arrow to go back",0,0);
            if(ofGetMousePressed()){
                kitchenInteractions=false;
            }
        }
    }
    
    else{
        pethunger.petinteract=false;
    }
    //this is the code for the oven
    if(kitchenObj == 2 && SceneCounter==3){
//        ofColor colorOne(7);
//        ofColor colorTwo(0);
//        ofBackgroundGradient(colorOne, colorTwo, OF_GRADIENT_CIRCULAR);
        camera.begin();
        ofPushMatrix();
        ofTranslate(-100,0);
        oven.draw();
        ofPopMatrix();
        camera.end  ();
        
    }

}


void ofApp::keyPressed(int key){
    
    //When the right key is presed, Scenecounter becomes 3(Kithceh nScene)
    if(key == OF_KEY_RIGHT){
        SceneCounter=3;
        kitchenObj=0;
    }
    //When left key is pressed, SceneCounter ==2 showing the art galley
    if(key == OF_KEY_LEFT){
        SceneCounter=2;
        kitchenObj=0;
    }
    //twhen the boolean is true, this rotates the globe only in scenecounter 2
    if(key == 'r'){
        interact = true;
        pethealth.petinteract=true;
    }

    
    //this stops the rotaion of the globe.
    if(key == 't'){
        interact = false;
    }
    //turn the light on like a switch
    if(key == OF_KEY_UP){
        roomlight=true;

    }
    //turn the light off like a switch
    if(key == OF_KEY_DOWN){
        roomlight=false;
    }

    //moves the pet in scene 2
if(key == 'a'){
    myCat.posision.x-=5.05;
    pethealth.petinteract=true;
    }
    //when key is pressed, moved towards the right on the x axis
if(key == 'd'){
    myCat.posision.x+=5.05;
    pethealth.petinteract=true;
    }
    //when w is pressed,moves upwards on the z axis
if(key == 'w'){
    myCat.posision.z-=5.05;
    pethealth.petinteract=true;
    }
   //moves down on the z axis
if(key == 's'){
    myCat.posision.z+=5.05;
    pethealth.petinteract=true;
    }

if(key == 32){
    kitchenInteractions=true;
    }
    
    


        
    

    


    

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
  //when the users releases the following keys(pet movesment), the petinteract boolean in the healthvar class will become false. When this is false, the value of AnimalHealth gets smaller which causes the healthbar to decrement over time
    if(SceneCounter == 2){
        if(key == 'a'){

            pethealth.petinteract=false;
        }
        //when the users releases the following keys(pet movesment), the petinteract boolean in the healthvar class will become false. When this is false, the value of AnimalHealth gets smaller which causes the healthbar to decrement over time
        if(key == 'd'){
            pethealth.petinteract=false;
        }
        //when the users releases the following keys(pet movesment), the petinteract boolean in the healthvar class will become false. When this is false, the value of AnimalHealth gets smaller which causes the healthbar to decrement over time
        if(key == 'w'){

            pethealth.petinteract=false;
        }
        //when the users releases the following keys(pet movesment), the petinteract boolean in the healthvar class will become false. When this is false, the value of AnimalHealth gets smaller which causes the healthbar to decrement over time
        if(key == 's'){
            pethealth.petinteract=false;
        }
        //when the users releases the following keys(pet movesment), the petinteract boolean in the healthvar class will become false. When this is false, the value of AnimalHealth gets smaller which causes the healthbar to decrement over time
        if(key == 't'){
            pethealth.petinteract=false;
        }

    }
    //when the users releases the following keys(pet movesment), the petinteract boolean in the healthvar class will become false. When this is false, the value of AnimalHealth gets smaller which causes the healthbar to decrement over time
    if(SceneCounter == 3){
        if(key == 'a'){
            pethealth.petinteract=false;
        }
        //when the users releases the following keys(pet movesment), the petinteract boolean in the healthvar class will become false. When this is false, the value of AnimalHealth gets smaller which causes the healthbar to decrement over time
        if(key == 'd'){
            pethealth.petinteract=false;
            
        }
        //when the users releases the following keys(pet movesment), the petinteract boolean in the healthvar class will become false. When this is false, the value of AnimalHealth gets smaller which causes the healthbar to decrement over time
        if(key == 'w'){

            pethealth.petinteract=false;
        }
        //when the users releases the following keys(pet movesment), the petinteract boolean in the healthvar class will become false. When this is false, the value of AnimalHealth gets smaller which causes the healthbar to decrement over time
        if(key == 's'){
            pethealth.petinteract=false;
        }
        
    }


    
 



}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
 

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    //this is for the menu scene, when the user clicks the mouse, the Scenecounter =1 meaning a new scene will be drawn in the draw function.
    if(SceneCounter==0){
        SceneCounter=1;
    }

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}

void ofApp::addQuadFacet(ofMesh &curMesh, vec3 v0, vec3 v1, vec3 v2, vec3 v3, vec2 tMin, vec2 tMax) {
    // Adds a quadrilateral facet based on positions of the 4 vertex positions and min and max
    // values for the texture coordinates. Automatically calculates the normal based on a CCW winding
    // order for the vertex positions.

    int baseIndex = curMesh.getNumVertices();

    curMesh.addVertex(v0);
    curMesh.addVertex(v1);
    curMesh.addVertex(v2);
    curMesh.addVertex(v3);

    vec3 n = normalize(cross(v1 - v0, v3 - v0));
    curMesh.addNormal(n);
    curMesh.addNormal(n);
    curMesh.addNormal(n);
    curMesh.addNormal(n);

    curMesh.addTexCoord(vec2(tMin.x, tMin.y));
    curMesh.addTexCoord(vec2(tMax.x, tMin.y));
    curMesh.addTexCoord(vec2(tMax.x, tMax.y));
    curMesh.addTexCoord(vec2(tMin.x, tMax.y));

    curMesh.addTriangle(baseIndex, baseIndex + 1, baseIndex + 2);
    curMesh.addTriangle(baseIndex, baseIndex + 2, baseIndex + 3);
}

ofVboMesh ofApp::createCuboidMesh(float width, float height) {
    ofVboMesh curMesh;

    curMesh.setMode(OF_PRIMITIVE_TRIANGLES);

    // Pedestal width and height values
    const float w = 0.5f * width;
    const float h = height;

    // Facets for the 4 sides
    addQuadFacet(curMesh, vec3(-w, 0, w), vec3(w, 0, w), vec3(w, h, w), vec3(-w, h, w), vec2(0.0, 0.0), vec2(0.25, 1.0));
    addQuadFacet(curMesh, vec3(w, 0, w), vec3(w, 0, -w), vec3(w, h, -w), vec3(w, h, w), vec2(0.25, 0.0), vec2(0.5, 1.0));
    addQuadFacet(curMesh, vec3(w, 0, -w), vec3(-w, 0, -w), vec3(-w, h, -w), vec3(w, h, -w), vec2(0.5, 0.0), vec2(0.75, 1.0));
    addQuadFacet(curMesh, vec3(-w, 0, -w), vec3(-w, 0, w), vec3(-w, h, w), vec3(-w, h, -w), vec2(0.75, 0.0), vec2(1.0, 1.0));

    // Facet for the top
    addQuadFacet(curMesh, vec3(-w, h, w), vec3(w, h, w), vec3(w, h, -w), vec3(-w, h, -w), vec2(0.0, 0.0), vec2(0.25, 0.25));

    // Facet for the bottom
    addQuadFacet(curMesh, vec3(-w, 0, w), vec3(-w, 0, -w), vec3(w, 0, -w), vec3(w, 0, w), vec2(0.25, 0.25), vec2(0.5, 0.5));

    return curMesh;
}

void ofApp::TeapotAndPedestalSetup(){
    //teapot initialize
    teapotMaterial.setDiffuseColor(ofFloatColor(1.0, 1.0, 1.0));
    teapotMaterial.setSpecularColor(ofFloatColor(0.7, 0.7, 0.7));
    teapotMaterial.setShininess(50.0);
    teapotMaterial.setAmbientColor(ofFloatColor(0.4, 0.4, 0.4));
    
    
  
    //pedesta material setup
    
    pedestaMesh=createCuboidMesh(16,50);
    pedestaMaterial.setSpecularColor(ofFloatColor(0.3,0.3,0.3));
    pedestaMaterial.setShininess(10.0);
    ofEnableNormalizedTexCoords();
    woodTexture.load("woodTexA.jpg");
    
    
    //pedesta material setup
    
    pedestaMesh1=createCuboidMesh(16,50);
    pedestaMaterial1.setSpecularColor(ofFloatColor(0.3,0.3,0.3));
    pedestaMaterial1.setShininess(10.0);
    ofEnableNormalizedTexCoords();
    woodTexture.load("woodTexA.jpg");
    
}


void ofApp::TeapotAndPedestalDraw(){
    //First Teapot
    ofTranslate(0,-60);
    ofPushMatrix();
    ofTranslate(-50, 60);
    teapotMaterial.begin();
    teapotMesh.draw();
    teapotMaterial.end();
    ofPopMatrix();
    
    //teapot stand
    ofPushMatrix();
    ofTranslate(-50, 0);
    woodTexture.bind();
    pedestaMaterial.begin();
    pedestaMesh.draw();
    pedestaMaterial.end();
    woodTexture.unbind();
    ofPopMatrix();
    
    

//pedestal with different dimenstion which will be found in scene 2.
    for(int i=0;i<20;i++){
        ofPushMatrix();
        ofTranslate(30+i, 0+i,0);
        woodTexture.bind();
        pedestaMaterial1.begin();
        pedestaMesh1.draw();
        pedestaMaterial1.end();
        woodTexture.unbind();
        ofPopMatrix();
        
    }
    
}

void ofApp::CameraAndLightingSetup(){
    //we setup and camera features here. We store it into a function as we will call it multiple times. This contains large quantity of code hence why i have stored it here.
    camera.setDistance(1);
    camera.setPosition(vec3(20.0, 70.0, 140.0));
    camera.setTarget(vec3(0.0, 0.0, 0.0));
    camera.setFov(35.0);
    camera.setAutoDistance(false);
    camera.setDistance(100);
    
    //we enalbe our light here and set the posision to the centre of the canvas.
    light.enable();
    light.setPosition(0, 00, 0);
    
    //lights setup
    keyLight.setDiffuseColor(ofFloatColor(1.0, 1.0, 1.0));
    keyLight.setSpecularColor(keyLight.getDiffuseColor());
    keyLight.setPosition(vec3(120.0, 100.0, 120.0));
}

void ofApp::StartGame(){
    //this is the function of the Scene One. After the game has loaded, this is called which will display all the floor,cat painitng and doorss
    CatRange();
    ofColor colorOne(7);
    ofColor colorTwo(0);
    ofBackgroundGradient(colorOne, colorTwo, OF_GRADIENT_CIRCULAR);
    
    ofSetColor(255,255,255);
    ofDrawBitmapString(": Use Up and Down Arrows to turn on and off light, Press r to rotate Globe and t to stop", ofGetWidth()/2-210, 20);
    ofDrawBitmapString(": Double click mouse to get a first person view of painting and objects", ofGetWidth()/2-210, 40);
    
    ofDrawBitmapString(": Use letters A,S,W and D to interact with PET", ofGetWidth()/2-210, 60);
    
    myCat.VirtualPos();


    
    //we enable camera interation here
    ofEnableDepthTest();
    camera.begin();
    ofEnableLighting();
   
    
    
    //we draw our floor here, i did not use aa class as its a simple 3d primitive being used.
    texture.bind();
    floor.draw();
    texture.unbind();
    
    //to keep the code clean, i created a fuction which will be called in draw. This function has ann the code for the Tepot and Pedestal
    TeapotAndPedestalDraw();
    
//we draw our painting using our pain class.
    ofPushMatrix();
    ofTranslate(30, 60,60);
    myPaint.draw();
    ofPopMatrix();
    
    //we draw our globe here, we centre it ontop of our second parting.
    ofPushMatrix();
    ofTranslate(50, 83,0);
    if(interact == true){
        rotateGlobe-=1;
        ofRotateYDeg(rotateGlobe);
    }
    myGlobe.draw();
    ofPopMatrix();
    

    myCat.game();
    
    position.x=0;
    position.y=-70;
    position.z=-95;
    
    floor.setPosition(position.x, position.y, position.z);
    floor.setDepth(270);
    floor.setHeight(5);
    floor.setWidth(200);
    //we close our camera activity.
    ofDisableLighting();
    camera.end();
    //this calls the function which checks all range of the cat.
    
    


}

void ofApp::KitchenSetup(){
    camera.setDistance(250);
    woodTexture.load("woodTexA.jpg");
    teapotMesh.load("teapot.ply");
    room.setPosition(0, 0, 0); //sets the room position
    cupboard2.setPosition(30, -35, 19); //sets the second pair of cupboards position
    cupboard3.setPosition(-20, 20, 0); //sets the third pair of cupboards position
}

void ofApp::KitchenDraw(){
    CatRange();
    ofSetColor(255,255,255);
    ofDrawBitmapString(": Use Up and Down Arrows to turn on and off light, Press r to rotate Globe and t to stop", ofGetWidth()/2-210, 20);
    ofDrawBitmapString(": Double click mouse to get a first person view of painting and objects", ofGetWidth()/2-210, 40);

    ofDrawBitmapString(": Use letters A,S,W and D to interact with PET", ofGetWidth()/2-210, 60);

    myCat.VirtualPos();
    
    
    
    ofPushMatrix();
    ofEnableDepthTest();
    camera.begin();
//    ofEnableLighting();
    myCat.game();
    ofTranslate(0, 0,-200);
    keyLight.enable();
//    ofTranslate(0, -60,0); //moves everything down by 50px
    
    

    //this section draws the teapot and moves the teapot 10px up
    ofPushMatrix();
        ofTranslate(0, 10);
        teapotMaterial.begin();
        teapotMesh.draw();
        teapotMaterial.end();
    ofPopMatrix();

    room.draw(); //draws the room

    //this section draws the cupboards and moves it 2px down and 10px to the left
    ofPushMatrix();
        ofTranslate(-10, -2, 0);
        cupboard.draw();
        cupboard2.draw();


    kit_draw.draw(); //draws the first draw

    //this section draws the remaing 3 draws and positions them with translate
    ofPushMatrix();
        ofTranslate(-8, 0, 0);
        kit_draw2.draw();

        ofTranslate(18, 0, 0);
        kit_draw3.draw();

        ofTranslate(-130, 30, -140);
        fridge.draw();
        fridge.doors();
        ofPopMatrix();

        kit_draw4.draw();
    ofPopMatrix();

    micro.draw(); //draws the microwave
    oven.draw(); //draws the oven

    //this section disables everything associated with the camera and lighting
    keyLight.disable();
    ofDisableLighting();
    camera.end();
    ofDisableDepthTest();
    
    gui.draw(); //draws the gui
    ofPopMatrix();
    
}


void ofApp::CatRange(){
    
    //Kitchen Walking in from Scene2
    ofPushMatrix();
    //this checks if the pet goes through scene 2 door to the kitchen
    if(SceneCounter == 2){
        if(myCat.posision.z<=-130&&myCat.posision.z>=-181){
            if(myCat.posision.x<=95&&myCat.posision.x>=80){
                SceneCounter=3;
            }
        }
    }
    ofPopMatrix();
    
    
    
    ofPushMatrix();
    if(SceneCounter==3){
        //if the pet goes through the door1 from the kitchen, goes back to Scene2 ie the art gallery.
        if(myCat.posision.z>=-29&&myCat.posision.z<=15){
            if(myCat.posision.x<=-106&&myCat.posision.x>=-116.15){
                SceneCounter=2;
            }
        }
        
        if(myCat.posision.z>=-131&&myCat.posision.z<=-86){
            if(myCat.posision.x<=-111.0&&myCat.posision.x>=-116.15){
                SceneCounter=4;
            }

        }
        //this open the fridge object.
        if(myCat.posision.x>=-116.00&&myCat.posision.x<=-80.5){
            if(myCat.posision.z>=-176&&myCat.posision.z<=-151){
                kitchenObj=1;
                myCat.posision.x=0;
                myCat.posision.z=0;
                pethunger.petinteract=true;
            }
        }
        else{
            kitchenObj=0;
        }
    }
    ofPopMatrix();
    
    if(SceneCounter == 4){
        if(myCat.posision.z>=300){
            SceneCounter=3;
        }
    }
    
    

    
    

    

    
   
    
    
    
    
    
}

