#pragma once
#include "ofMain.h"
class kitchen_fridge
{

public:
	kitchen_fridge();
	~kitchen_fridge();
	
	void update();
	void draw();
	void setPosition(float x, float y, float z);
    void opened();
    void doors();

	
	
private:

	float posX, posY, posZ;
	//creates box primitives 
    ofBoxPrimitive	door, handle1, handle2,second_door, second_handle1, second_handle2, body,fridgeBody,fridgeBody1,door1,door2;
	
	//creates cylinder primitives
	ofCylinderPrimitive handle3, second_handle3;
    ofSpherePrimitive apple;

	//creates textures
	ofTexture door_texture, handle_texture,fridgetexture,appletexture,doors1;
	
	//creates materials
	ofMaterial door_material, handle_material;

};

